#!/bin/bash

LOG_PREFIX="[Dune Node]"
COMMAND_NAME=dune-node

DUNE_SRCDIR=$(dirname $(dirname $0))
SCRIPTS_DIR=$DUNE_SRCDIR/run-scripts
source $SCRIPTS_DIR/set-variables.sh

stop_dune_node () {
    $SCRIPTS_DIR/stop-process.sh
}

status_dune_node () {
    $SCRIPTS_DIR/check-status.sh
}

head_dune_node () {
    $DUNE_CMDDIR/dune-client -A $DUNE_RPC_ADDR -P $DUNE_RPC_PORT rpc get /chains/main/blocks/head/header/
}

client_dune_node () {
    $DUNE_CMDDIR/dune-client -base-dir $DUNE_CLIDIR -A $DUNE_RPC_ADDR -P $DUNE_RPC_PORT "$@"
}

clean_dune_node () {
    rm -rf $DUNE_CLIDIR/
    rm -rf $DUNE_DATADIR/context/
    rm -rf $DUNE_DATADIR/store/
}

activate_dune_node() {

    if [ -f $GENESIS_DIR/genesis-env.sh ]; then
	source $GENESIS_DIR/genesis-env.sh
	mkdir -p $DUNE_CLIDIR
	
	sed \
	    -e "s/NAME/activator/" \
	    -e "s/KEYHASH/$ACTIVATOR_KEYHASH/" \
	    $SCRIPTS_DIR/client/public_key_hashs \
	    > $DUNE_CLIDIR/public_key_hashs
	sed \
	    -e "s/NAME/activator/" \
	    -e "s/PUBKEY/$ACTIVATOR_PUBKEY/" \
	    $SCRIPTS_DIR/client/public_keys \
	    > $DUNE_CLIDIR/public_keys
	sed \
	    -e "s/NAME/activator/" \
	    -e "s/PRIVKEY/$ACTIVATOR_PRIVKEY/" \
	    $SCRIPTS_DIR/client/secret_keys \
	    > $DUNE_CLIDIR/secret_keys
	
	PROTO_SHORT_HASH=$(echo $PROTO_HASH | cut -b 1-8)

	$DUNE_CMDDIR/dune-client -base-dir $DUNE_CLIDIR --addr $DUNE_RPC_ADDR --port $DUNE_RPC_PORT -block genesis activate protocol $PROTO_HASH with fitness 1 and key activator and parameters $GENESIS_DIR/protocol_parameters.json

    else
	echo You have to set GENESIS_DIR in '$HOME/dune-env.sh'. Example:
	echo GENESIS_DIR='$HOME/GIT/dune-network/genesis/testnet'
    fi
}

bake_dune_node() {

    if [ -f $GENESIS_DIR/genesis-env.sh ]; then
	source $GENESIS_DIR/genesis-env.sh
	mkdir -p $DUNE_CLIDIR
	
	sed \
	    -e "s/NAME/baker/" \
	    -e "s/KEYHASH/$BAKER_KEYHASH/" \
	    $SCRIPTS_DIR/client/public_key_hashs \
	    > $DUNE_CLIDIR/public_key_hashs
	sed \
	    -e "s/NAME/baker/" \
	    -e "s/PUBKEY/$BAKER_PUBKEY/" \
	    $SCRIPTS_DIR/client/public_keys \
	    > $DUNE_CLIDIR/public_keys
	sed \
	    -e "s/NAME/baker/" \
	    -e "s/PRIVKEY/$BAKER_PRIVKEY/" \
	    $SCRIPTS_DIR/client/secret_keys \
	    > $DUNE_CLIDIR/secret_keys
	
	PROTO_SHORT_HASH=$(echo $PROTO_HASH | cut -b 1-8)

	$DUNE_CMDDIR/dune-client -base-dir $DUNE_CLIDIR --addr $DUNE_RPC_ADDR --port $DUNE_RPC_PORT bake for baker

    else
	echo You have to set GENESIS_DIR in '$HOME/dune-env.sh'. Example:
	echo GENESIS_DIR='$HOME/GIT/dune-network/genesis/testnet'
    fi
}

start_dune_node () {

    $SCRIPTS_DIR/check-status.sh

    if [ $? -eq 1 ]; then
    
	DUNE_ROTATE=$DUNE_SRCDIR/dune-rotate
	export DUNE_ROTATE

	mkdir -p $DUNE_LOGDIR
	mkdir -p $DUNE_RUNDIR
	mkdir -p $DUNE_CMDDIR

	cp -f $DUNE_SRCDIR/dune-node $DUNE_CMDDIR/
	cp -f $DUNE_SRCDIR/dune-client $DUNE_CMDDIR/
	
	if test -f $INIFILE; then
            echo Dune node already initialized
	else
            echo Initializing Dune node
            $DUNE_CMDDIR/$COMMAND_NAME identity generate --data-dir $DUNE_DATADIR
	fi
	
	$DUNE_ROTATE $LOGFILE
	$DUNE_CMDDIR/$COMMAND_NAME run --data-dir $DUNE_DATADIR --net-addr [::]:$DUNE_NET_PORT --rpc-addr $DUNE_RPC_ADDR:$DUNE_RPC_PORT --cors-header=content-type --cors-origin=* --history-mode=$DUNE_HISTORY_MODE --bootstrap-threshold $DUNE_BOOTSTRAP_THRESHOLD "$@" >& $LOGFILE &
	echo $! > $PIDFILE
	echo "Dune node started..."
	sleep 1
	$SCRIPTS_DIR/check-status.sh
	echo "You can watch the log:"
	echo tail -f $LOGFILE
    fi
}

case "$1" in
    start)
        shift
        echo "$LOG_PREFIX == Starting =="
        start_dune_node "$@"
	;;

    debug)
        shift
        echo "$LOG_PREFIX == Starting =="
        DUNE_LOG='* -> debug'
        export DUNE_LOG
        start_dune_node "$@"
	;;

    stop)
        echo "$LOG_PREFIX == Stopping =="
        stop_dune_node
        ;;

    restart)
        echo "$LOG_PREFIX == Restarting =="
        echo "$LOG_PREFIX == Stopping =="
        stop_dune_node
        echo "$LOG_PREFIX == Starting =="
        start_dune_node
        ;;

    status)
        echo "$LOG_PREFIX == Status == "
        status_dune_node
        ;;

    clean)
        echo "$LOG_PREFIX == Clean == "
        echo "$LOG_PREFIX == Stopping =="
        stop_dune_node
        echo "$LOG_PREFIX == Cleaning =="
        clean_dune_node
        ;;

    head)
        echo "$LOG_PREFIX == HEAD == "
	head_dune_node
        ;;

    activate)
        echo "$LOG_PREFIX == ACTIVATE == "
	activate_dune_node
        ;;

    bake)
        echo "$LOG_PREFIX == BAKE == "
	bake_dune_node
        ;;

    client)
        shift
        echo "$LOG_PREFIX == CLIENT ==  $@ "
	client_dune_node "$@"
        ;;

    *)
        echo "Usage: ./dune-node.sh {start|stop|restart|status|head|activate|client}"
        exit 1
        ;;

esac

exit 0
