variables:
  ## Please update `scripts/version.sh` accordingly
  build_deps_image_version: c7111eec0e5d08e38864abf7767dcd8f766c99fd
  build_deps_image_name: registry.gitlab.com/dune-network/opam-repository
  public_docker_image_name: registry.gitlab.com/${CI_PROJECT_PATH}

stages:
  - doc
  - build
  - unittest
  - integration
  - documentation
  - packaging
  - publish


############################################################
## Stage: build (only MR)                                 ##
############################################################

.build_template: &build_definition
  image: ${build_deps_image_name}:${build_deps_image_version}
  stage: build
  except:
    - master
    - alphanet
    - zeronet
    - mainnet
    - alphanet-staging
    - zeronet-staging
    - mainnet-staging
    - zeronet-snapshots
    - mainnet-snapshots
  before_script:
    - opam list
    - . ./scripts/version.sh

check_opam_deps:
  <<: *build_definition
  script:
    - if [ "${build_deps_image_version}" != "${opam_repository_tag}" ] ; then
        echo "Inconsistent dependencies hash between 'scripts/version.sh' and '.gitlab-ci.yml'." ;
        exit 1 ;
      fi
    - ./scripts/opam-check.sh
    - ./scripts/check_opam_test.sh

check_docker_network:
  <<: *build_definition
  script:
    - ./scripts/check_docker_network_test.sh

check_opam_lint:
  <<: *build_definition
  script:
    - find . ! -path "./_opam/*" -name "*.opam" -exec opam lint {} +;

check_indentation:
  <<: *build_definition
  script:
    - dune build @runtest_indent

build:
  <<: *build_definition
  script:
    - dune build @runtest_dune_template
    - make all
  artifacts:
    paths:
    - _build
    expire_in: 1 day


############################################################
## Stage: test (only MR)                                  ##
############################################################

.test_template: &test_definition
  <<: *build_definition
  stage: unittest
  dependencies:
    - build
  variables:
    DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER: "y"
    DUNE_TZ_COMPATIBILITY: "y"

test:stdlib:
  <<: *test_definition
  script:
    - dune build @src/lib_stdlib/runtest

test:stdlib_unix:
  <<: *test_definition
  script:
    - dune build @src/lib_stdlib_unix/runtest

test:data_encoding:
  <<: *test_definition
  script:
    - dune build @src/lib_data_encoding/runtest

test:storage:
  <<: *test_definition
  script:
    - dune build @src/lib_storage/runtest

test:crypto:
  <<: *test_definition
  script:
    - dune build @src/lib_crypto/runtest

test:shell:
  <<: *test_definition
  script:
    - make dune-make-version
    - dune build @src/lib_shell/runtest

test:p2p:io-scheduler:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_io_scheduler_ipv4

test:p2p:socket:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_socket_ipv4

test:p2p:pool:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_pool_ipv4

test:proto_alpha:lib_protocol:
  <<: *test_definition
  script:
    - dune build @src/proto_004_Pt24m4xi/lib_protocol/runtest

test:proto_alpha:lib_client:
  <<: *test_definition
  script:
    - dune build @src/proto_004_Pt24m4xi/lib_client/test/runtest

test:p2p:peerset:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_peerset

test:p2p:ipv6set:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_ipv6set

test:p2p:banned_peers:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_banned_peers

test:client_alpha:vote:
  <<: *test_definition
  script:
    - make dune-make-version
    - dune build @src/proto_004_Pt24m4xi/lib_delegate/runtest_vote

test:validation:
  <<: *test_definition
  script:
    - dune build @src/lib_validation/runtest

test:micheline:
  <<: *test_definition
  script:
    - dune build @src/lib_micheline/runtest

############################################################
## Stage: run shell integration tests                     ##
############################################################

# definition for the environment to run all integration tests
.integration_template: &integration_definition
  <<: *build_definition
  stage: integration
  dependencies:
    - build
  before_script:
    - sudo apk add py-pip python3 python3-dev
    - sudo pip install --upgrade pip
    - sudo pip3 install -r tests_python/requirements.txt
    - make all
  variables:
    DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER: "y"
    DUNE_TZ_COMPATIBILITY: "y"

test:basic.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_basic.sh

test:contracts.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts.sh

test:contracts_opcode.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts_opcode.sh

test:contracts_macros.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts_macros.sh

test:contracts_mini_scenarios.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts_mini_scenarios.sh

test:multinode.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_multinode.sh

test:inject.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_injection.sh

test:voting.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_voting.sh

test:proto:sandbox:
  <<: *integration_definition
  script:
    - dune build @runtest_sandbox

############################################################
## Stage: run OCaml integration tests                     ##
############################################################

test:sandboxes:voting:
  <<: *integration_definition
  script:
    - ROOT_PATH=$PWD/flextesa-voting-demo dune build @src/bin_flextesa/runtest_sandbox_voting_demo
  artifacts:
    paths:
    - flextesa-voting-demo
    expire_in: 1 day
    when: on_failure
  allow_failure: true # This test uses too much resources for GitLab's workers

test:sandboxes:acc-baking:
  <<: *integration_definition
  script:
    - ROOT_PATH=$PWD/flextesa-acc-sdb dune build @src/bin_flextesa/runtest_sandbox_accusations_simple_double_baking
  artifacts:
    paths:
    - flextesa-acc-sdb
    expire_in: 1 day
    when: on_failure

test:sandboxes:acc-endorsement:
  <<: *integration_definition
  script:
    - ROOT_PATH=$PWD/flextesa-acc-sde dune build @src/bin_flextesa/runtest_sandbox_accusations_simple_double_endorsing
  artifacts:
    paths:
    - flextesa-acc-sde
    expire_in: 1 day
    when: on_failure

############################################################
## Stage: run python integration tests                    ##
############################################################

integration:linter_python:
  <<: *integration_definition
  script:
    - make -C tests_python lint_all

##BEGIN_INTEGRATION_PYTHON##
integration:baker_endorser:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_baker_endorser.py

integration:basic:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_basic.py

integration:contract:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_contract.py

integration:contract_baker:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_contract_baker.py

integration:injection:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_injection.py

integration:many_bakers:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_many_bakers.py

integration:many_nodes:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_many_nodes.py

integration:mempool:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_mempool.py

integration:multinode:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_multinode.py

integration:rpc:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_rpc.py

integration:voting:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_voting.py

integration:tls:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_tls.py

integration:cors:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_cors.py

##END_INTEGRATION_PYTHON##

############################################################
## Stage: run doc integration tests                       ##
############################################################

.test:documentation:
  <<: *test_definition
  stage: documentation
  script:
    - sudo apk add --no-cache py3-sphinx py3-sphinx_rtd_theme
    - sudo pip3 uninstall 'idna' --yes ## Fix up dependencies in alpine:3.8
    - sudo pip3 install 'idna<2.7'
    - sudo ln -s /usr/bin/sphinx-build-3 /usr/bin/sphinx-build
    - make doc-html

# test:linkcheck:
#   <<: *test_definition
#   stage: documentation
#   script:
#     - sudo apk add --no-cache py3-sphinx py3-sphinx_rtd_theme
#     - sudo pip3 uninstall 'idna' --yes ## Fix up dependencies in alpine:3.8
#     - sudo pip3 install 'idna<2.7'
#     - sudo ln -s /usr/bin/sphinx-build-3 /usr/bin/sphinx-build
#     - make doc-html-and-linkcheck
#   allow_failure: true

############################################################
## Stage: building opam packages (only master and *opam*) ##
############################################################

.opam_template: &opam_definition
  image: ${build_deps_image_name}:opam--${build_deps_image_version}
  stage: packaging
  dependencies: []
  only:
    - master
    - /^.*opam.*$/
  script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}

##BEGIN_OPAM##
opam:00:ocplib-json-typed:
  <<: *opam_definition
  variables:
    package: ocplib-json-typed

opam:01:ocplib-json-typed-bson:
  <<: *opam_definition
  variables:
    package: ocplib-json-typed-bson

opam:02:tezos-stdlib:
  <<: *opam_definition
  variables:
    package: tezos-stdlib

opam:03:tezos-data-encoding:
  <<: *opam_definition
  variables:
    package: tezos-data-encoding

opam:04:ocplib-resto:
  <<: *opam_definition
  variables:
    package: ocplib-resto

opam:05:tezos-error-monad:
  <<: *opam_definition
  variables:
    package: tezos-error-monad

opam:06:ocplib-resto-directory:
  <<: *opam_definition
  variables:
    package: ocplib-resto-directory

opam:07:blake2:
  <<: *opam_definition
  variables:
    package: blake2

opam:08:hacl:
  <<: *opam_definition
  variables:
    package: hacl

opam:09:secp256k1:
  <<: *opam_definition
  variables:
    package: secp256k1

opam:10:tezos-clic:
  <<: *opam_definition
  variables:
    package: tezos-clic

opam:11:tezos-rpc:
  <<: *opam_definition
  variables:
    package: tezos-rpc

opam:12:uecc:
  <<: *opam_definition
  variables:
    package: uecc

opam:13:tezos-crypto:
  <<: *opam_definition
  variables:
    package: tezos-crypto

opam:14:tezos-event-logging:
  <<: *opam_definition
  variables:
    package: tezos-event-logging

opam:15:tezos-micheline:
  <<: *opam_definition
  variables:
    package: tezos-micheline

opam:16:lmdb:
  <<: *opam_definition
  variables:
    package: lmdb

opam:17:tezos-base:
  <<: *opam_definition
  variables:
    package: tezos-base

opam:18:pbkdf:
  <<: *opam_definition
  variables:
    package: pbkdf

opam:19:ocplib-resto-cohttp:
  <<: *opam_definition
  variables:
    package: ocplib-resto-cohttp

opam:20:irmin-lmdb:
  <<: *opam_definition
  variables:
    package: irmin-lmdb

opam:21:tezos-shell-services:
  <<: *opam_definition
  variables:
    package: tezos-shell-services

opam:22:tezos-stdlib-unix:
  <<: *opam_definition
  variables:
    package: tezos-stdlib-unix

opam:23:bip39:
  <<: *opam_definition
  variables:
    package: bip39

opam:24:tezos-rpc-http:
  <<: *opam_definition
  variables:
    package: tezos-rpc-http

opam:25:tezos-storage:
  <<: *opam_definition
  variables:
    package: tezos-storage

opam:26:ledgerwallet:
  <<: *opam_definition
  variables:
    package: ledgerwallet

opam:27:tezos-client-base:
  <<: *opam_definition
  variables:
    package: tezos-client-base

opam:28:ledgerwallet-tezos:
  <<: *opam_definition
  variables:
    package: ledgerwallet-tezos

opam:29:tezos-signer-services:
  <<: *opam_definition
  variables:
    package: tezos-signer-services

opam:30:tezos-protocol-environment-sigs:
  <<: *opam_definition
  variables:
    package: tezos-protocol-environment-sigs

opam:31:tezos-signer-backends:
  <<: *opam_definition
  variables:
    package: tezos-signer-backends

opam:32:tezos-protocol-environment:
  <<: *opam_definition
  variables:
    package: tezos-protocol-environment

opam:33:tezos-client-commands:
  <<: *opam_definition
  variables:
    package: tezos-client-commands

opam:34:tezos-protocol-compiler:
  <<: *opam_definition
  variables:
    package: tezos-protocol-compiler

opam:35:tezos-protocol-environment-shell:
  <<: *opam_definition
  variables:
    package: tezos-protocol-environment-shell

opam:36:tezos-client-base-unix:
  <<: *opam_definition
  variables:
    package: tezos-client-base-unix

opam:37:tezos-protocol-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-protocol-004-Pt24m4xi

opam:38:tezos-protocol-updater:
  <<: *opam_definition
  variables:
    package: tezos-protocol-updater

opam:39:tezos-client-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-client-004-Pt24m4xi

opam:40:tezos-p2p:
  <<: *opam_definition
  variables:
    package: tezos-p2p

opam:41:tezos-validation:
  <<: *opam_definition
  variables:
    package: tezos-validation

opam:42:tezos-baking-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-baking-004-Pt24m4xi

opam:43:tezos-protocol-000-Ps9mPmXa:
  <<: *opam_definition
  variables:
    package: tezos-protocol-000-Ps9mPmXa

opam:44:tezos-protocol-demo:
  <<: *opam_definition
  variables:
    package: tezos-protocol-demo

opam:45:ocplib-resto-json:
  <<: *opam_definition
  variables:
    package: ocplib-resto-json

opam:46:tezos-embedded-protocol-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-embedded-protocol-004-Pt24m4xi

opam:47:tezos-shell:
  <<: *opam_definition
  variables:
    package: tezos-shell

opam:48:tezos-baking-004-Pt24m4xi-commands:
  <<: *opam_definition
  variables:
    package: tezos-baking-004-Pt24m4xi-commands

opam:49:tezos-client-000-Ps9mPmXa:
  <<: *opam_definition
  variables:
    package: tezos-client-000-Ps9mPmXa

opam:50:tezos-client-004-Pt24m4xi-commands:
  <<: *opam_definition
  variables:
    package: tezos-client-004-Pt24m4xi-commands

opam:51:tezos-client-demo:
  <<: *opam_definition
  variables:
    package: tezos-client-demo

opam:52:ocplib-ezresto:
  <<: *opam_definition
  variables:
    package: ocplib-ezresto

opam:53:tezos-embedded-protocol-000-Ps9mPmXa:
  <<: *opam_definition
  variables:
    package: tezos-embedded-protocol-000-Ps9mPmXa

opam:54:tezos-embedded-protocol-demo:
  <<: *opam_definition
  variables:
    package: tezos-embedded-protocol-demo

opam:55:tezos-mempool-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-mempool-004-Pt24m4xi

opam:56:tezos-baker-004-Pt24m4xi-commands:
  <<: *opam_definition
  variables:
    package: tezos-baker-004-Pt24m4xi-commands

opam:57:tezos-client:
  <<: *opam_definition
  variables:
    package: tezos-client

opam:58:ocp-external-callback:
  <<: *opam_definition
  variables:
    package: ocp-external-callback

opam:59:ocplib-ezresto-directory:
  <<: *opam_definition
  variables:
    package: ocplib-ezresto-directory

opam:60:tezos-config:
  <<: *opam_definition
  variables:
    package: tezos-config

opam:61:tezos-accuser-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-accuser-004-Pt24m4xi

opam:62:tezos-endorser-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-endorser-004-Pt24m4xi

opam:63:tezos-accuser-004-Pt24m4xi-commands:
  <<: *opam_definition
  variables:
    package: tezos-accuser-004-Pt24m4xi-commands

opam:64:dune-gen:
  <<: *opam_definition
  variables:
    package: dune-gen

opam:65:dune-rotate:
  <<: *opam_definition
  variables:
    package: dune-rotate

opam:66:tezos-baker-004-Pt24m4xi:
  <<: *opam_definition
  variables:
    package: tezos-baker-004-Pt24m4xi

opam:67:tezos-endorser-004-Pt24m4xi-commands:
  <<: *opam_definition
  variables:
    package: tezos-endorser-004-Pt24m4xi-commands

opam:68:dune-test:
  <<: *opam_definition
  variables:
    package: dune-test

opam:69:tezos-network-sandbox:
  <<: *opam_definition
  variables:
    package: tezos-network-sandbox

opam:70:tezos-signer:
  <<: *opam_definition
  variables:
    package: tezos-signer

opam:71:tezos-node:
  <<: *opam_definition
  variables:
    package: tezos-node

opam:72:ocplib-json-typed-browser:
  <<: *opam_definition
  variables:
    package: ocplib-json-typed-browser

opam:73:dune-forge:
  <<: *opam_definition
  variables:
    package: dune-forge


##END_OPAM##



############################################################
## Stage: publish                                         ##
############################################################

publish:docker:
  image: docker:latest
  services:
    - docker:18-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375/
  stage: publish
  only:
    - master@dune-network/dune-network
    - next@dune-network/dune-network
    - CI@dune-network/dune-network
    - mainnet@dune-network/dune-network
    - mainnet-staging@dune-network/dune-network
    - mainnet-snapshots@dune-network/dune-network
    - testnet@dune-network/dune-network
    - testnet-staging@dune-network/dune-network
    - testnet-snapshots@dune-network/dune-network
  before_script:
    - apk add git
    - mkdir ~/.docker || true
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - LAST_COMMIT_DATE_TIME=$(git log --pretty=format:"%cd" -1 --date="format:%Y%m%d%H%M%S" 2>&1)
    - ./scripts/create_docker_image.sh
        "${public_docker_image_name}" "${CI_COMMIT_REF_NAME}"
    - docker push "${public_docker_image_name}:${CI_COMMIT_REF_NAME}"
    - docker tag "${public_docker_image_name}:${CI_COMMIT_REF_NAME}" "${public_docker_image_name}:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
    - docker push "${public_docker_image_name}:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
  tags:
    - docker

.publish:doc:
  image: ${build_deps_image_name}:${build_deps_image_version}
  stage: doc
  only:
    - master@tezos/tezos
    - alphanet@tezos/tezos
    - zeronet@tezos/tezos
    - mainnet@tezos/tezos
  before_script:
    - sudo apk add --no-cache py3-sphinx py3-sphinx_rtd_theme openssh-client rsync
    - sudo pip3 uninstall 'idna' --yes ## Fix up dependencies in alpine:3.8
    - sudo pip3 install 'idna<2.7'
    - sudo ln -s /usr/bin/sphinx-build-3 /usr/bin/sphinx-build
    - echo "${CI_PK_GITLAB_DOC}" > ~/.ssh/id_ed25519
    - echo "${CI_KH}" > ~/.ssh/known_hosts
    - chmod 400 ~/.ssh/id_ed25519
  script:
    - make doc-html
    - git clone git@gitlab.com:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAMESPACE}.gitlab.io gitlab.io
    - rsync --recursive --links --perms --delete --verbose
        --exclude=.doctrees
        docs/_build/ gitlab.io/public/"${CI_COMMIT_REF_NAME}"
    - cd gitlab.io
    - if [ -z "$(git status -s)" ] ; then
        echo "Nothing to commit!" ;
      else
        git add public/"${CI_COMMIT_REF_NAME}" ;
        git commit -m "Import doc for ${CI_COMMIT_REF_NAME} (${CI_COMMIT_SHA})" ;
        git push origin master ;
      fi
