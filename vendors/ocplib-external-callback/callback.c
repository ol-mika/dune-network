/**************************************************************************/
/*  This program is free software: you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  any later version.                                                    */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful,       */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
/**************************************************************************/

#include <caml/mlvalues.h>
#include <caml/callback.h>
#include <caml/memory.h>
#include <caml/misc.h>
#include <caml/alloc.h>
#include <caml/fail.h>


CAMLprim value ocp_external_callback_v(value name_v, value arg_v)
{
  CAMLparam2(name_v, arg_v);
  CAMLlocal1(ret);
  char *name = String_val(name_v);
  ret = caml_callback(*caml_named_value(name), arg_v);
  CAMLreturn(ret);
}
