Dune Network
============

Mainnet: [![pipeline status](https://gitlab.com/dune-network/dune-network/badges/mainnet/pipeline.svg)](https://gitlab.com/dune-network/dune-network/commits/mainnet)
Testnet: [![pipeline status](https://gitlab.com/dune-network/dune-network/badges/testnet/pipeline.svg)](https://gitlab.com/dune-network/dune-network/commits/testnet)
Next: [![pipeline status](https://gitlab.com/dune-network/dune-network/badges/next/pipeline.svg)](https://gitlab.com/dune-network/dune-network/commits/next)

This is the official implementation of the [Dune
Network](https://dune.network) protocol in OCaml.

Community
---------

* Dune Foundation:

  * https://dune.network

* Block Explorers:

  * https://dunscan.io

Networks
--------

Dune has two running networks:

  * Mainnet is the official network. Use branch `mainnet` in this repository.

  * Testnet is running the same software as Mainnet, but with fake accounts.
    It can be used to test transactions, smart contracts, etc.
    Use the branch `testnet` in this repository
    You can create fake accounts using: https://faucet.dune.network/

Branches
--------

In this repository, you will find the following branches:

  * `mainnet`: the code running on the Mainnet network
  * `testnet`: the code running on the Testnet network. Very close to `mainnet`,
    except for some configuration
  * `next`: the development branch, also configured for `testnet`. When stable
    and tested, this branch will land into `mainnet` and `testnet`

Licenses
--------

Dune is free software, released under the [GPLv3
License](LICENSES/GPLv3.txt). The original Tezos code base is released
under the terms of the [DLS Open Source
License](LICENSES/DLS-OSL.txt).

See [LICENSE.txt](LICENSE.txt).


Acknowledgments 
--------

Dune Network's software is based on Tezos' software. We are thankful
to OCamlPro, Nomadic Labs and all other Tezos contributors.
