#! /bin/sh

## `ocaml-version` should be in sync with `README.rst` and
## `lib.protocol-compiler/tezos-protocol-compiler.opam`

ocaml_version=4.07.1
opam_version=2.0

## Please update `.gitlab-ci.yml` accordingly
opam_repository_tag=c7111eec0e5d08e38864abf7767dcd8f766c99fd
full_opam_repository_tag=c7111eec0e5d08e38864abf7767dcd8f766c99fd
opam_repository_url=https://gitlab.com/dune-network/opam-repository.git
opam_repository=$opam_repository_url\#$opam_repository_tag
