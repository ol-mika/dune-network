#! /bin/sh

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

. "$script_dir"/version.sh

# getting version
dune exec "${script_dir#$PWD/}"/get_p2p_version/get_p2p_version.exe >"$script_dir"/alphanet_version
