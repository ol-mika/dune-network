(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type config = {
  network : string ;
  forced_protocol_upgrades : (Int32.t * string) list;
  genesis_key : string ;
  genesis_time : string ;
  genesis_block : string ;
  genesis_protocol : string ;
  p2p_version : string ;
  max_operation_data_length : int ;
  encoding_version : int ;
  prefix_dir : string ; (* "dune" or "tezos" *)
  p2p_port : int ; (* 9732 *)
  rpc_port : int ; (* 8732 *)
  bootstrap_peers  : string list ; (* [ "boot.tzbeta.net" ] *)
  cursym : string ;
  tzcompat : bool ;
}
