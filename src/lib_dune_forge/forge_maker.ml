(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Forge_types

let set_uint8 cs pos v = Cstruct.set_uint8 cs pos v; pos+1
let set_uint32 cs pos v = Cstruct.BE.set_uint32 cs pos v; pos+4

let set_z cs pos v =
  let rec iter pos v =
    if v > 127 then
      let x = (v land 0x7f) lor 0x80 in
      let pos = set_uint8 cs pos x in
      let v = v lsr 7 in
      iter pos v
    else
      set_uint8 cs pos v
  in
  iter pos v

let set_bytes cs pos bytes =
  Cstruct.blit_from_bytes bytes 0 cs pos (Bytes.length bytes);
  pos + Bytes.length bytes

let set_string cs pos bytes =
  Cstruct.blit_from_string bytes 0 cs pos (String.length bytes);
  pos + String.length bytes


let set_implicit_contract cs pos (ContractHash pkh) =
  let pos = set_uint8 cs pos 0 in
  let pos = set_string cs pos pkh in
  pos

(*
+==================================+======================+=====================================+
+----------------------------------+----------------------+-------------------------------------+
| watermark                        | 1 byte               | 0x03                                |
+----------------------------------+----------------------+-------------------------------------+
| branch                           | 32 bytes             | block hash                          |
+----------------------------------+----------------------+-------------------------------------+
| Tag = 8                          | 1 byte               | unsigned 8-bit integer              |
+----------------------------------+----------------------+-------------------------------------+
| source                           | 22 bytes             | $contract_id                        |
+----------------------------------+----------------------+-------------------------------------+
| fee                              | Determined from data | $N.t                                |
+----------------------------------+----------------------+-------------------------------------+
| counter                          | Determined from data | $N.t                                |
+----------------------------------+----------------------+-------------------------------------+
| gas_limit                        | Determined from data | $N.t                                |
+----------------------------------+----------------------+-------------------------------------+
| storage_limit                    | Determined from data | $N.t                                |
+----------------------------------+----------------------+-------------------------------------+
| amount                           | Determined from data | $N.t                                |
+----------------------------------+----------------------+-------------------------------------+
| destination                      | 22 bytes             | $contract_id                        |
+----------------------------------+----------------------+-------------------------------------+
| ? presence of field "parameters" | 1 byte               | boolean (0 for false, 255 for true) |
+----------------------------------+----------------------+-------------------------------------+
| parameters                       | Determined from data | $fitness.elem                       |
+----------------------------------+----------------------+-------------------------------------+
*)

let forge_manager_operation cs pos (ManagerOperation (branch, ops)) =
  let BlockHash hash = branch in
  let pos = set_string cs pos hash in (* branch *)
  let rec iter pos ops =
    match ops with
    [] -> pos
    | Transaction tr :: ops ->
        let pos = set_uint8 cs pos 0x08 in (* transaction *)
        let pos = set_implicit_contract cs pos tr.tr_source in (* source *)
        let pos = set_z cs pos tr.tr_fee in   (* fees *)
        let pos = set_z cs pos tr.tr_counter in      (* counter *)
        let pos = set_z cs pos tr.tr_gas_limit in  (* gas *)
        let pos = set_z cs pos tr.tr_storage_limit in  (* storage *)
        let pos = set_z cs pos tr.tr_amount in      (* amount *)
        let pos = set_implicit_contract cs pos tr.tr_destination in (* dest *)
        let pos = match tr.tr_parameters with
          | None -> set_uint8 cs pos 0
          | Some _michelson -> assert false (* NOT IMPLEMENTED *)
        in
        iter pos ops
  in
  iter pos ops

let with_cstruct ?(length=10_000) f =
  let cs = Cstruct.create length in
  let pos = 0 in
  let pos = f cs pos in
  Cstruct.sub cs 0 pos

let to_hexa cs =
  let b = Buffer.create (2*Cstruct.len cs) in
  Cstruct.hexdump_to_buffer b cs;
  Buffer.contents b

let to_string cs = Cstruct.to_string cs
