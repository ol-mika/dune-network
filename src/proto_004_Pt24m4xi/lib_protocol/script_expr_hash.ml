(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let script_expr_hash = "\013\044\064\027" (* expr(54) *)

include Blake2B.Make(Base58)(struct
    let name = "script_expr"
    let title = "A script expression ID"
    let b58check_prefix = script_expr_hash
    let size = None
  end)

let () =
  Base58.check_encoded_prefix b58check_encoding "expr" 54

type error += Invalid_code_notation of string (* `Permanent *)

let of_b58check s =
  match Base58.decode s with
  | Some (Data h) -> ok h
  | _ -> error (Invalid_code_notation s)

let () =
  let open Data_encoding in
  register_error_kind
    `Permanent
    ~id:"code.invalid_code_notation"
    ~title: "Invalid code notation"
    ~pp: (fun ppf x -> Format.fprintf ppf "Invalid code notation %S" x)
    ~description:
      "A malformed code notation was given to an RPC or in a script."
    (obj1 (req "notation" string))
    (function Invalid_code_notation loc -> Some loc | _ -> None)
    (fun loc -> Invalid_code_notation loc)
