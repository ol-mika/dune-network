(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type error += Unsupported_revision of int * int
type error += Bad_revision_change of int * int

let () =
  register_error_kind
    `Permanent
    ~id:"protocol_revision.unsupported"
    ~title:"Unsupported protocol revision"
    ~description:"The protocol revision is not supported by this node."
    ~pp:(fun ppf (revision, max_revision) ->
        Format.fprintf ppf
          "This node does not support protocol %d (max is %d)"
          revision max_revision
      )
    Data_encoding.(obj2 (req "revision" int31) (req "max_revision" int31))
    (function Unsupported_revision (r, m) -> Some (r, m) | _ -> None)
    (fun (r, m) -> Unsupported_revision (r, m)) ;
  register_error_kind
    `Permanent
    ~id:"protocol_revision.illegal_change"
    ~title:"Bad protocol revision change"
    ~description:"The protocol revision cannot be changed to a lower value."
    ~pp:(fun ppf (revision, prev_revision) ->
        Format.fprintf ppf
          "Cannot set protocol revision to a lower value (%d, already at revision %d)"
          revision prev_revision
      )
    Data_encoding.(obj2 (req "new_revision" int31) (req "current_revision" int31))
    (function Bad_revision_change (r, p) -> Some (r, p) | _ -> None)
    (fun (r, p) -> Bad_revision_change (r, p)) ;
  ()


(* Maximum supported revision for this protocol *)
let max_revision = 0

module Int32 = struct
  type t = Int32.t
  let encoding = Data_encoding.int32
end

(* Storage *)

module Activate_protocol = struct

  module Raw_context =
    Storage_functors.Make_subcontext(Raw_context)
      (struct let name = ["activate_protocol"] end)

  module Level =
    Storage_functors.Make_single_data_storage(Raw_context)
      (struct let name = [ "level" ] end)
      (Int32)

  module Protocol =
    Storage_functors.Make_single_data_storage(Raw_context)
      (struct let name = [ "protocol" ] end)
      (Protocol_hash)

  module Protocol_parameters =
    Storage_functors.Make_single_data_storage(Raw_context)
      (struct let name = [ "protocol_parameters" ] end)
      (struct
        type t = Dune_parameters_repr.parameters
        let encoding = Dune_parameters_repr.parameters_encoding
      end)

end

(* Accessors *)

let get_activate_protocol_level = Activate_protocol.Level.get_option
let get_activate_protocol_and_cleanup ctxt =
  Activate_protocol.Level.remove ctxt >>= fun ctxt ->
  Activate_protocol.Protocol.get_option ctxt >>=? fun protocol ->
  Activate_protocol.Protocol.remove ctxt >>= fun ctxt ->
  Activate_protocol.Protocol_parameters.get_option ctxt >>=?
  fun protocol_parameters ->
  Activate_protocol.Protocol_parameters.remove ctxt >>= fun ctxt ->
  return ( ctxt, protocol, protocol_parameters )

let set_activate_protocol
    ctxt ?protocol ?protocol_parameters level =
  begin match protocol with
    | Some protocol ->
        Activate_protocol.Protocol.init_set ctxt protocol
    | None -> Activate_protocol.Protocol.remove ctxt
  end >>= fun ctxt ->
  begin match protocol_parameters with
    | None -> Activate_protocol.Protocol_parameters.remove ctxt
    | Some protocol_parameters ->
        Activate_protocol.Protocol_parameters.init_set ctxt protocol_parameters
  end >>= fun ctxt ->
  Activate_protocol.Level.init_set ctxt level

let genesis_pubkey =
  Signature.Public_key.of_b58check_exn Config.config.genesis_key


module Foundation_pubkey =
  Storage_functors.Make_single_data_storage
    (Raw_context)
    (struct let name = [ "foundation_pubkey" ] end)
    (Signature.Public_key)


let get_foundation_pubkey t =
  Foundation_pubkey.get t >>= function
  | Ok x -> Lwt.return x
  | Error _ -> Lwt.return genesis_pubkey

let set_foundation_pubkey = Foundation_pubkey.init_set

let patch_constants = Raw_context.patch_constants

module Protocol_revision =
  Storage_functors.Make_single_data_storage
    (Raw_context)
    (struct let name = [ "protocol_revision" ] end)
    (struct
      type t = int
      let encoding = Data_encoding.int31
    end)

let get_protocol_revision t =
  Protocol_revision.get t >>= function
  | Ok x -> Lwt.return x
  | Error _ -> Lwt.return 0

let set_protocol_revision t revision =
  get_protocol_revision t >>= fun prev_rev ->
  (* We cannot do all these checks. A protocol might set the revision for the
    next protocol. For example, we might be at (ProtoA, rev 1), and jump
    to (ProtoB, rev 0) or to (ProtoB, rev 2), even if ProtoA only has one
    2 revisions (max_revision), not 3

  fail_when Compare.Int.(revision < prev_rev)
    (Bad_revision_change (revision, prev_rev)) >>=? fun () ->
  fail_when Compare.Int.(revision > max_revision)
    (Unsupported_revision (revision, max_revision)) >>=? fun () ->
*)
  if Compare.Int.(revision = prev_rev) then
    return t
  else begin
    Protocol_revision.init_set t revision >>= return
  end

let check_protocol_revision t =
  get_protocol_revision t >>= fun revision ->
  fail_when Compare.Int.(revision > max_revision)
    (Unsupported_revision (revision, max_revision))

(* Signature re-exported by Alpha_context, to avoid multiple
   redefinitions of types and values *)
module type S = sig
  type context
  type dune_parameters
  type parametric

  val get_foundation_pubkey : context -> Signature.Public_key.t Lwt.t
  val set_foundation_pubkey : context -> Signature.Public_key.t -> context Lwt.t

  val set_activate_protocol : context ->
    ?protocol:Protocol_hash.t ->
    ?protocol_parameters:dune_parameters ->
    int32 ->
    context Lwt.t
  val get_activate_protocol_level :
    context -> int32 option tzresult Lwt.t
  val get_activate_protocol_and_cleanup :
    context ->
    (context * Protocol_hash.t option * dune_parameters option) tzresult Lwt.t

  val patch_constants : context ->
    (parametric -> parametric) -> context Lwt.t

  val get_protocol_revision : context -> int Lwt.t
  val set_protocol_revision : context -> int -> context tzresult Lwt.t
  val check_protocol_revision : context -> unit tzresult Lwt.t

end
