(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Proto_alpha
open Alpha_context
open Client_proto_context
open Client_proto_contracts
open Client_proto_args
open Client_proto_context_commands

let destinations_encoding =
  let open Data_encoding in
  list
    (obj4
       (req "address" Contract.encoding)
       (req "amount" Tez.encoding)
       (opt "collect_fee_gas" z)
       (opt "arg" string))

let read_destinations_file filename =
  Lwt_utils_unix.Json.read_file filename >>=? fun json ->
  match Data_encoding.Json.destruct destinations_encoding json with
  | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
      Format.kasprintf (fun s -> failwith "%s" s)
        "Invalid destinations file: %a %a"
        (fun ppf -> Data_encoding.Json.print_error ppf) exn
        Data_encoding.Json.pp json
  | destinations ->
      return destinations

open Clic

let batch_transfer_command =
  command ~group:Client_proto_context_commands.group
    ~desc: "Batch transfer tokens."
    (args14 fee_arg dry_run_switch verbose_signing_switch
       gas_limit_arg storage_limit_arg counter_arg no_print_source_flag
       minimal_fees_arg
       minimal_nanotez_per_byte_arg
       minimal_nanotez_per_gas_unit_arg
       force_low_fee_arg
       fee_cap_arg
       burn_cap_arg
       batch_size_arg)
    (prefixes [ "dune"; "batch"; "transfer" ]
     @@ tez_param
       ~name: "total" ~desc: "total amount taken from source"
     @@ prefix "from"
     @@ ContractAlias.destination_param
       ~name: "src" ~desc: "name of the source contract"
     @@ prefix "to"
     @@ string
       ~name: "file"
       ~desc:
         "JSON file with destinations and amounts\nof the form\n\
          [ {\"address\": \"dn1...\", \"amount\": \"10000\", \"arg\": \"Unit\"}, ... ]\n\
          The field \"amount\" is given in mutez, and \"arg\" is optional."
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, no_print_source, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap, batch_size)
      total (_, source) destination_filename cctxt ->
      source_to_keys cctxt
        ~chain:cctxt#chain ~block:cctxt#block
        source >>=? fun (src_pk, src_sk) ->
      let fee_parameter = {
        Injection.minimal_fees ;
        minimal_nanotez_per_byte ;
        minimal_nanotez_per_gas_unit ;
        force_low_fee ;
        fee_cap ;
        burn_cap ;
      } in
      read_destinations_file destination_filename >>=? fun destinations ->
      Client_proto_dune_context.batch_transfer cctxt
        ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
        ~dry_run ~verbose_signing
        ~fee_parameter ?batch_size
        ~source ?fee ~src_pk ~src_sk ~destinations ~total ?gas_limit ?storage_limit ?counter () >>=
      report_michelson_errors ~no_print_source ~msg:"transfer simulation failed" cctxt >>= function
      | None -> return_unit
      | Some (_res, _contracts) ->
          return_unit
    end


let protocol_group =
  { Clic.name = "Dune Protocol" ;
    title = "Dune Protocol specific commands" }

let file_parameter =
  Clic.parameter (fun _ p ->
      if not (Sys.file_exists p) then
        failwith "File doesn't exist: '%s'" p
      else
        return p)

let activate_protocol_command =
  command ~group:protocol_group ~desc: "Activate a protocol"
    Client_proto_context_commands.(
      args12 fee_arg dry_run_switch verbose_signing_switch
        gas_limit_arg storage_limit_arg counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    (prefixes [ "dune"; "activate" ; "protocol" ]
     @@ Protocol_hash.param ~name:"version" ~desc:"Protocol version (b58check)"
     @@ prefixes [ "with" ; "key" ]
     @@ ContractAlias.destination_param
       ~name: "activator" ~desc: "Activator's key"
     @@ prefixes [ "and" ; "parameters" ]
     @@ param ~name:"parameters"
       ~desc:"Protocol parameters (as JSON file)"
       file_parameter
     @@ prefixes [ "at" ; "level" ]
     @@ (param
           ~name: "level"
           ~desc: "Level"
           (parameter (fun _ctx s ->
                try return (Int32.of_string s)
                with _ -> failwith "%s is not an int32 value" s)))
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap)
      protocol (_, source) param_json_file level
      (cctxt : Proto_alpha.full) ->
      source_to_keys cctxt
        ~chain:cctxt#chain ~block:cctxt#block
        source >>=? fun (src_pk, src_sk) ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      (* TODO Dune: check whether `Shell_services.Protocol.list cctxt`
         contains the protocol *)
      >>=? fun json ->
      match Data_encoding.Json.destruct
              Dune_parameters.parameters_encoding json
      with
      | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
          fail
            (failure "Invalid parameters: %a %a"
               (fun ppf -> Data_encoding.Json.print_error ppf) exn
               Data_encoding.Json.pp json)
      |  protocol_parameters ->
          let fee_parameter = {
            Injection.minimal_fees ;
            minimal_nanotez_per_byte ;
            minimal_nanotez_per_gas_unit ;
            force_low_fee ;
            fee_cap ;
            burn_cap ;
          } in
          Client_proto_dune_context.activate_protocol_operation cctxt
            ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
            ~dry_run ~verbose_signing
            ~fee_parameter
            ~level ~protocol ~protocol_parameters
            ~source ?fee ~src_pk ~src_sk  ?gas_limit ?storage_limit ?counter ()
          >>=? fun _res ->
          return_unit
    end

let manage_accounts_command =
  command ~group:protocol_group ~desc: "Manage accounts"
    Client_proto_context_commands.(
      args12 fee_arg dry_run_switch verbose_signing_switch
        gas_limit_arg storage_limit_arg counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    (prefixes [ "dune"; "manage" ; "accounts" ]
     @@ prefixes [ "with" ; "key" ]
     @@ ContractAlias.destination_param
       ~name: "activator" ~desc: "Activator's key"
     @@ prefixes [ "and" ; "parameters" ]
     @@ param ~name:"parameters"
       ~desc:"Accounts parameters (as JSON file)"
       file_parameter
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap)
      (_, source) param_json_file
      (cctxt : Proto_alpha.full) ->
      source_to_keys cctxt
        ~chain:cctxt#chain ~block:cctxt#block
        source >>=? fun (src_pk, src_sk) ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      >>=? fun json ->
      let protocol_parameters = Data_encoding.Binary.to_bytes_exn Data_encoding.json json in
      let fee_parameter = {
        Injection.minimal_fees ;
        minimal_nanotez_per_byte ;
        minimal_nanotez_per_gas_unit ;
        force_low_fee ;
        fee_cap ;
        burn_cap ;
      } in
      Client_proto_dune_context.manage_accounts_operation cctxt
        ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
        ~dry_run ~verbose_signing
        ~fee_parameter
        ~protocol_parameters
        ~source ?fee ~src_pk ~src_sk  ?gas_limit ?storage_limit ?counter ()
      >>=? fun _res ->
      return_unit
    end

let commands () =
  [
    batch_transfer_command ;
    activate_protocol_command ;
    manage_accounts_command ;
  ]
