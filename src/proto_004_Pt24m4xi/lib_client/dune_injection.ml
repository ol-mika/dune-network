(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Proto_alpha
open Alpha_context
open Apply_results

let rec extract_contents_result :
  type kind. kind contents_and_result_list -> kind contents_result_list
  = function
    | Single_and_result (_, cr) -> Single_result cr
    | Cons_and_result (_, cr, rest) -> Cons_result (cr, extract_contents_result rest)

let inject_batch_manager_operations
    cctxt ~chain ~block ?branch ?confirmations ?dry_run ?verbose_signing
    ~source ~src_pk ~src_sk ?fee ?(gas_limit = Z.minus_one)
    ?(storage_limit = (Z.of_int (-1))) ?counter ~fee_parameter
    (operations : packed_manager_operation list) =
  begin
    match counter with
    | None ->
        Alpha_services.Contract.counter
          cctxt (chain, block) source >>=? fun pcounter ->
        let counter = Z.succ pcounter in
        return counter
    | Some counter ->
        return counter
  end >>=? fun counter ->
  Alpha_services.Contract.manager_key
    cctxt (chain, block) source >>=? fun (_, key) ->
  let is_reveal : type kind. kind manager_operation -> bool = function
    | Reveal _ -> true
    | _ -> false in
  let compute_fee, fee =
    match fee with
    | None -> true, Tez.zero
    | Some fee -> false, fee in
  let contents_of_manager_operation counter
      (type kind) (operation : kind manager_operation)
    : kind Kind.manager contents = match operation with
    | Reveal _ as operation ->
        Manager_operation { source ; fee = Tez.zero ; counter ;
                            gas_limit = Z.of_int 10_000 ;
                            storage_limit = Z.zero ;
                            operation }
    | operation ->
        Manager_operation { source ; fee ; counter ;
                            gas_limit ; storage_limit ; operation } in
  let rec make_operation_list counter = function
    | [] -> [], Z.pred counter
    | [Manager operation] ->
        [Contents (contents_of_manager_operation counter operation)], counter
    | Manager operation :: operations ->
        let operation_list, last_counter =
          make_operation_list (Z.succ counter) operations in
        (Contents (contents_of_manager_operation counter operation) ::
         operation_list), last_counter in
  let operations, add_reveal = match key, operations with
    | None, Manager operation :: _ when not (is_reveal operation) ->
        Manager (Reveal src_pk) :: operations, true
    | _ -> operations, false in
  let operation_list, last_counter = make_operation_list counter operations in
  let Contents_list contents = Operation.of_list operation_list in
  Injection.inject_operation cctxt ~chain ~block ?confirmations ?dry_run
    ~fee_parameter ~compute_fee
    ?verbose_signing ?branch ~src_sk contents >>=? fun (oph, op, result) ->
  let packed_results = match pack_contents_list op result with
    | Cons_and_result (_, _, rest) when add_reveal ->
        Contents_result_list (extract_contents_result rest)
    | pcr ->
        Contents_result_list (extract_contents_result pcr) in
  return (oph, to_list packed_results, last_counter)
