(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Proto_alpha
open Alpha_context
open Tezos_micheline
open Dune_lang (* Michelson_v1_* = Dune_lang_v1_* *)

let parse_expression arg =
  Lwt.return
    (Micheline_parser.no_parsing_error
       (Michelson_v1_parser.parse_expression arg))

let batch_transfer (cctxt : #Proto_alpha.full)
    ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ~src_pk ~src_sk ~destinations ~total
    ?fee ?gas_limit ?storage_limit ?counter ?batch_size
    ~fee_parameter
    () =
  let add_to_current_batch op = function
    | [] -> [[op]]
    | batch :: rest -> (op :: batch) :: rest in
  let add_to_new_batch op = function
    | [] -> [[op]]
    | batch :: rest -> [op] :: List.rev batch :: rest in
  let reorder_batches = function
    | [] -> []
    | batch :: rest -> List.rev (List.rev batch :: rest) in
  fold_left_s
    (fun (operations, total, current_batch_size) (destination, amount, collect_fee_gas, arg) ->
       begin match arg with
         | Some arg ->
             parse_expression arg >>=? fun { expanded = arg ; _ } ->
             return_some arg
         | None -> return_none
       end >>=? fun parameters ->
       let parameters = Option.map ~f:Script.lazy_expr parameters in
       let op = Manager (Transaction { amount ; parameters ; destination ; collect_fee_gas }) in
       Lwt.return (Alpha_environment.wrap_error Tez.( amount +? total )) >>|? fun total ->
       let operations, current_batch_size = match batch_size with
         | Some batch_size when current_batch_size >= batch_size ->
             add_to_new_batch op operations, 1
         | _ -> add_to_current_batch op operations, current_batch_size + 1 in
       operations, total, current_batch_size
    ) ([], Tez.zero, 0) destinations >>=? fun (operations, ops_total, _) ->
  fail_unless Tez.( total = ops_total )
    (failure "@[<v 2>Total doesn't match:@ \
              Expected: %a@ \
              Got: %a @]"
       Tez.pp total Tez.pp ops_total) >>=? fun () ->
  let operations = reorder_batches operations in
  fold_left_s (fun (ophs, results, counter) batch ->
      Dune_injection.inject_batch_manager_operations
        cctxt ~chain ~block ~confirmations:0 (* Wait after *)
        ?dry_run ?verbose_signing
        ?branch ~source ?fee ?gas_limit ?storage_limit ?counter
        ~src_pk ~src_sk
        ~fee_parameter
        batch >>|? fun (oph, result, last_counter) ->
      oph :: ophs, result :: results, Some (Z.succ last_counter)
    ) ([], [], counter) operations >>=? fun (ophs, results, _counter) ->
  let ophs = List.rev ophs in
  let result = List.fold_left (fun acc r -> List.rev_append r acc) [] results in
  begin match confirmations, dry_run with
    | None, _ | _, Some true -> return_unit
    | Some confirmations, _ ->
        cctxt#message
          "@[<v 2>Waiting for the following operations to be included...@,%a@]"
          (Format.pp_print_list Operation_hash.pp) ophs
        >>= fun () ->
        iter_p (fun oph ->
            Client_confirmations.wait_for_operation_inclusion cctxt
              ~predecessors:(10 + List.length ophs)
              ~chain (* ?branch *) ~confirmations oph >>=? fun _ ->
            return_unit
          ) ophs
  end >>=? fun () ->
  let Contents_result_list result_list = Apply_results.of_list result in
  Lwt.return
    (Injection.originated_contracts result_list) >>=? fun contracts ->
  return (result, contracts)


let activate_protocol_operation (cctxt : #Proto_alpha.full)
    ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ~src_pk ~src_sk

    ~level
    ?protocol
    ?protocol_parameters (* operation specifics *)

    ?fee ?gas_limit ?storage_limit ?counter
    ~fee_parameter
    () =
  let contents = Dune_manager_operation
      ( Dune_activate_protocol { level ; protocol ; protocol_parameters } )
  in
  Injection.inject_manager_operation
    cctxt ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ?fee ?gas_limit ?storage_limit ?counter
    ~src_pk ~src_sk
    ~fee_parameter
    contents >>=? fun (_oph, _op, result as res) ->
  Lwt.return
    (Injection.originated_contracts (Single_result result)) >>=?
  fun contracts ->
  return (res, contracts)

let manage_accounts_operation (cctxt : #Proto_alpha.full)
    ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ~src_pk ~src_sk

    ~protocol_parameters (* operation specifics *)

    ?fee ?gas_limit ?storage_limit ?counter
    ~fee_parameter
    () =
  let contents = Dune_manager_operation
      ( Dune_manage_accounts protocol_parameters )
  in
  Injection.inject_manager_operation
    cctxt ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ?fee ?gas_limit ?storage_limit ?counter
    ~src_pk ~src_sk
    ~fee_parameter
    contents >>=? fun (_oph, _op, result as res) ->
  Lwt.return
    (Injection.originated_contracts (Single_result result)) >>=?
  fun contracts ->
  return (res, contracts)
