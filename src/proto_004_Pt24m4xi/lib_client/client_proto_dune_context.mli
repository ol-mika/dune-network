(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Proto_alpha
open Alpha_context

val batch_transfer :
  #Proto_alpha.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:public_key ->
  src_sk:Client_keys.sk_uri ->
  destinations:(Contract.t * Tez.t * Z.t option * string option) list ->
  total:Tez.t ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  ?batch_size:int ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  (Apply_results.packed_contents_result list * Contract.t list) tzresult Lwt.t

val activate_protocol_operation :
  #Proto_alpha.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->

  level: int32 ->
  ?protocol:Protocol_hash.t ->
  ?protocol_parameters:Dune_parameters.parameters ->

  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  ((Operation_hash.t *
    Kind.dune_manager_operation
      Kind.manager
      contents *
    Kind.dune_manager_operation
      Kind.manager
      Proto_alpha.Apply_results.contents_result) *
   Contract.t list)
    tzresult Lwt.t


val manage_accounts_operation :
  #Proto_alpha.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->

  protocol_parameters:MBytes.t ->

  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  ((Operation_hash.t *
    Kind.dune_manager_operation
      Kind.manager
      contents *
    Kind.dune_manager_operation
      Kind.manager
      Proto_alpha.Apply_results.contents_result) *
   Contract.t list)
    tzresult Lwt.t
