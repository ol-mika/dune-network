.. Dune documentation master file, created by
   sphinx-quickstart on Sat Nov 11 11:08:48 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to the Dune Developer Documentation!
=============================================

The Project
-----------

Dune is a distributed consensus platform with meta-consensus
capability. Dune not only comes to consensus about the state of its ledger,
like Bitcoin or Ethereum. It also attempts to come to consensus about how the
protocol and the nodes should adapt and upgrade.

 - Developer documentation is available online at
   https://dune.network/docs/dune-dev-docs .
   Make sure you are consulting the right version.
 - The website https://dune.network/ contains more information about the project.
 - All development happens on GitLab at https://gitlab.com/dune-network

The source code of Dune is placed under the GPLv3 License.


The Community
-------------

- The website of the `Dune Foundation <https://dune.network/foundation/>`_.
- `Dune sub-reddit <https://www.reddit.com/r/dune-network/>`_ is an
  important meeting point of the community.
- Several community-built block explorers are available:

    - https://dunscan.io
    - https://testnet.dunscan.io

- More resources can be found on https://docs.dune.network


The Networks
------------

Mainnet
~~~~~~~

The Dune network is the current incarnation of the Dune blockchain.

It runs with real DUNs from several sources:

* Foundators of Dune received tokens on June 24, 2019
* Tezos users received tokens through an airdrop in September 2019
* Tezos ICO participants can claim DUNs equivalent to their Tezos allocation (see :ref:`activate_fundraiser_account`).

The Dune network has been live and open since June 24th, 2019.

All the instructions in this documentation are valid for Mainnet
however we **strongly** encourage users to first try all the
introduction tutorials on Testnet to familiarize themselves without
risks.

Testnet
~~~~~~~

Dune Testnet is a test network for the Dune blockchain with a
faucet to obtain free DUNs (see :ref:`faucet`).
It is updated and rebooted rarely and it is running the same code as
the Mainnet.
It is the reference network for developers wanting to test their
software before going to beta and for users who want to familiarize
themselves with Dune before using their real DUNs.

We offer support for Testnet on IRC.


Getting started
---------------

The best place to start exploring the project is following the How Tos
in the :ref:`introduction <howtoget>`.

.. toctree::
   :maxdepth: 2
   :caption: Documentation:

   Dune Documentation <https://docs.dune.network>

.. toctree::
   :maxdepth: 2
   :caption: Introduction tutorials:

   introduction/howtoget
   introduction/howtouse
   introduction/howtorun

.. toctree::
   :maxdepth: 2
   :caption: User documentation:

   user/key-management
   user/sandbox
   user/history_modes
   user/snapshots
   user/various
   user/glossary

.. toctree::
   :maxdepth: 2
   :caption: White doc:

   whitedoc/the_big_picture
   whitedoc/p2p
   whitedoc/validation
   whitedoc/michelson
   whitedoc/proof_of_stake

.. toctree::
   :maxdepth: 2
   :caption: Developer Tutorials:

   developer/rpc
   developer/data_encoding
   developer/error_monad
   developer/michelson_anti_patterns
   developer/entering_alpha
   developer/protocol_environment
   developer/profiling
   developer/flextesa
   developer/contributing

.. toctree::
   :maxdepth: 2
   :caption: APIs:

   README
   api/api-inline
   api/cli-commands
   api/rpc
   api/errors
   api/p2p


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
